var io = require('socket.io').listen(1234),
    mysql = require('mysql'),
    pool = mysql.createPool({
        host     : 'localhost',
        user     : 'drivers',
        password : 'zaq1xsw2',
        database: 'drivers'
    });

var saveMessage = function (data) {
    var sql = "INSERT INTO message (user_id, channel_id, date, body) VALUES (?, ?, ?, ?)";
    sql = mysql.format(sql, data);

    pool.getConnection(function(err, connection) {

        if (err) {
            return;
        }

        connection.query(sql, function (err, result) {
            connection.release();
        });
    });

};

io.configure('production', function(){
    io.enable('browser client etag');
    io.set('log level', 1);

    io.set('transports', [
        'websocket',
        'flashsocket',
        'htmlfile',
        'xhr-polling',
        'jsonp-polling'
    ]);
});

io.sockets.on('connection', function (socket) {

    var room = null,
        roomId = null,
        fullName = null,
        userId = null;

    socket.on('subscribe', function(data) {
        room = data.room || null;
        fullName = data.full_name || null;
        roomId = data.room_id || null;
        userId = data.user_id || null;

        if (room !== null && fullName !== null && userId !== null && roomId !== null) {
            socket.join(room);
        } else {
            socket.disconnect();
        }
    });

    socket.on('message', function (message) {
        var date = new Date();

        io.sockets.in(room).emit('message', {
            user_full_name: fullName,
            message_body: message,
            message_date: date.getHours() + ":" + date.getMinutes()
        });

        saveMessage([
            userId,
            roomId,
            new Date(),
            message
        ]);
    });

    socket.on('unsubscribe', function() {
        socket.leave(room);
    });
});