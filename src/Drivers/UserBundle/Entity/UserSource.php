<?php

namespace Drivers\UserBundle\Entity;

class UserSource
{
    const VKONTAKTE = 'VKONTAKTE';
    const FACEBOOK = 'FACEBOOK';
    const APPLICATION = 'APPLICATION';
}