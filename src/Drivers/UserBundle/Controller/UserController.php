<?php

namespace Drivers\UserBundle\Controller;

use Drivers\UserBundle\Entity\User;
use Drivers\UserBundle\Form\ProfileFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

class UserController extends Controller
{
    /**
     * @Route("/profile", name="profile")
     * @Template()
     */
    public function profileAction(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $this->get('security.context')->getToken()->getUser();

        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);

        $form = $this->createForm(new ProfileFormType(), $user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /**
             * @var ObjectManager $em
             */
            $em = $this->getDoctrine()->getManager();

            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('profile'));
        }

        return [
            'form'  =>  $form->createView(),
            'user'  =>  $user
        ];
    }
}
