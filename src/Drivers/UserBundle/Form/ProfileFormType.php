<?php

namespace Drivers\UserBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'firstName',
                null,
                [
                    'attr' =>
                    [
                        'class' => 'input-block-level'
                    ],
                    'label' => 'Имя'
                ]
            )
            ->add(
                'lastName',
                null,
                [
                    'attr' =>
                    [
                        'class' => 'input-block-level'
                    ],
                    'label' => 'Фамилия'
                ]
            )
            ->add(
                'email',
                null,
                [
                    'constraints' =>
                    [
                        new NotBlank(),
                        new Email()
                    ],
                    'required' => true,
                    'attr' =>
                    [
                        'class' => 'input-block-level'
                    ]
                ]
            )
            ->add(
                'password',
                null,
                [
                    'label' =>  'Пароль (отображается в хешированном виде)',
                    'constraints' =>
                    [
                        new NotBlank(),
                    ],
                    'required' => true,
                    'attr' =>
                    [
                        'class' => 'input-block-level',
                    ]
                ]
            )
            ->add(
                'save',
                'submit',
                [
                    'label' =>  'Сохранить',
                    'attr' =>
                    [
                        'class' => 'btn btn-lg btn-success btn-block'
                    ]
                ]
            );
    }

    public function getName()
    {
        return 'profile';
    }
} 