<?php

namespace Drivers\ChatBundle\Controller;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Drivers\ChatBundle\Entity\Message;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Mvc;

/**
 * @Mvc\Route(service="drivers_chat.controller.chat")
 */
class ChatController
{
    private $doctrine;

    public function __construct(Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @Mvc\Route("/channels", name="channels")
     * @Mvc\Template()
     */
    public function channelsAction()
    {
        $em = $this->doctrine->getManager();

        $channels = $em->getRepository('Drivers\ChatBundle\Entity\Channel')->findAll();

        return [
            'channels' => $channels,
        ];
    }

    /**
     * @Mvc\Route("/channel/{slug}", name="channel", requirements={"slug" = "[a-zA-Z0-9-_]+"})
     * @Mvc\Template()
     */
    public function channelAction($slug)
    {
        $em = $this->doctrine->getManager();

        $channel = $em->getRepository('Drivers\ChatBundle\Entity\Channel')->findOneBy([
            'slug' => $slug
        ]);

        $messages = $em->getRepository('Drivers\ChatBundle\Entity\Message')->findForChannel($channel);

        return [
            'channel' => $channel,
            'messages' => $messages,
        ];
    }
}
