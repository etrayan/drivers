<?php

namespace Drivers\ChatBundle\Command;

use Ratchet\WebSocket\WsServer;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Ratchet\MessageComponentInterface;

class ChatCommand extends Command
{
    /**
     * @var MessageComponentInterface
     */
    private $chat;

    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $address;

    /**
     * @param MessageComponentInterface $chat
     * @param string $port
     * @param string $address
     */
    public function __construct(MessageComponentInterface $chat, $port, $address)
    {
        $this->chat = $chat;
        $this->port = $port;
        $this->address = $address;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('chat:start')
            ->setDescription('Starting websocket chat server')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ws = new WsServer($this->chat);
        $ws->disableVersion(0); // old, bad, protocol version

        $server = IoServer::factory(
            new HttpServer($ws),
            $this->port,
            $this->address
        );
        $server->run();
    }
} 