<?php

namespace Drivers\ChatBundle\Command;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearMessagesCommand extends Command
{
    const MESSAGE_ENTITY = 'DriversChatBundle:Message';
    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var integer
     */
    private $expiration;

    /**
     * @param Registry $doctrine
     * @param $expiration
     */
    public function __construct(Registry $doctrine, $expiration)
    {
        $this->doctrine = $doctrine;
        $this->expiration = $expiration;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('chat:clear')
            ->setDescription('Clearing old messages from chat')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->doctrine->getManager();
        $repository = $em->getRepository(self::MESSAGE_ENTITY);

        $expiredMessages = $repository->findExpired($this->expiration);

        foreach ($expiredMessages as $message) {
            $em->remove($message);
        }

        $em->flush();
    }
} 