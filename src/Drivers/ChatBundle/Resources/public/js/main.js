$(function() {
    Server.connect();
    Server.init();

    Chat.scroll();

    $('.send-message').on('click', Server.sendMessage);
});