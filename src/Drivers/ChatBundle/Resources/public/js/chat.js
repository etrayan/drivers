$(function(){

    (function(context, $){

        var $data = $('#data'),
            $error = $('.alert-error'),
            socket;

        var connect = function() {
            socket = io.connect($data.attr('data-dns'));
        };

        var init = function() {
            socket.on('connect', function () {
                socket.emit('subscribe', {
                    room: $data.attr('data-room-name'),
                    room_id: $data.attr('data-room-id'),
                    full_name: $data.attr('data-name'),
                    user_id: $data.attr('data-user')
                });
            });

            socket.on('connecting', function () {

            });

            socket.on('disconnect', function () {

            });

            socket.on('connect_failed', function () {
                $error
                    .show()
                    .find('.content')
                    .text('Ошибка в соединении. Попробуйте перезагрузить страницу');
            });

            socket.on('error', function () {
                $error
                    .show()
                    .find('.content')
                    .text('Неизвестная ошибка. Попробуйте перезагрузить страницу');
            });

            socket.on('message', function (message) {
                Chat.add(message);
                Chat.scroll();
            });

            socket.on('reconnect_failed', function () {
                $error
                    .show()
                    .find('.content')
                    .text('Не удалось переподключиться. Попробуйте перезагрузить страницу');
            });

            socket.on('reconnect', function () {

            });

            socket.on('reconnecting', function () {

            });
        };

        var sendMessage = function() {
            var message = MessageFactory.create();

            if (message) {
                socket.emit('message', message);
            }
        };

        context.Server = {
            connect: connect,
            init: init,
            sendMessage: sendMessage
        };
    }(window, jQuery));

    (function(context, $){
        var $message = $('.chat-message-field');

        var create = function() {
            var message = $message.val();

            $message.val('');

            if (message.length === 0) {
                return null;
            }

            return message;
        };

        context.MessageFactory =  {
            create: create
        }
    }(window, jQuery));

    (function(context, $){
        var $data = $('#data'),
            $messages = $('.messages'),
            $messagesBlock = $('#chat-messages'),
            template = $data.attr('data-prototype');

        var add = function(message) {
            $messages.append(Mustache.render(template, message));
        };

        var scroll = function() {
            $messagesBlock.scrollTop($messagesBlock[0].scrollHeight);
        };

        context.Chat =  {
            add: add,
            scroll: scroll
        }
    }(window, jQuery));
});
