<?php

namespace Drivers\ChatBundle\Entity;

use Doctrine\ORM\EntityRepository;

class MessageRepository extends EntityRepository
{
    public function findForChannel(Channel $channel)
    {
        $builder = $this->createQueryBuilder('m');
        $query = $builder
            ->where('m.channel = :channel')
            ->andWhere('m.date > :yesterday')
            ->setParameter('channel', $channel)
            ->setParameter('yesterday', new \DateTime('today'))
            ->orderBy('m.date', 'ASC')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @param $expiration
     * @return array
     */
    public function findExpired($expiration)
    {
        $expired = (new \DateTime())->modify(
            sprintf('-%d day 00:00', $expiration)
        );

        $builder = $this->createQueryBuilder('m');
        $query = $builder
            ->where('m.date < :expired')
            ->setParameter('expired', $expired)
            ->getQuery();

        return $query->getResult();
    }
} 