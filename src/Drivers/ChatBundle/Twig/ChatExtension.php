<?php

namespace Drivers\ChatBundle\Twig;

class ChatExtension extends \Twig_Extension
{
    /**
     * @var string
     */
    private $port;

    /**
     * @var string
     */
    private $address;

    public function __construct($port, $address)
    {
        $this->port = $port;
        $this->address = $address;
    }

    public function getName()
    {
        return 'chat';
    }

    public function getFunctions()
    {
        return [
            'websocket_dns' => new \Twig_Function_Method($this, 'getWebSocketDns'),
        ];
    }

    public function getWebSocketDns()
    {
        return sprintf(
            'http://%s:%s',
            $this->address,
            $this->port
        );
    }
} 