<?php

namespace Drivers\ChatBundle\Service\Chat;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Drivers\ChatBundle\Entity\Channel;
use Drivers\ChatBundle\Entity\Message;
use Drivers\UserBundle\Entity\User;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Doctrine\Common\Persistence\ObjectManager;

class Chat implements MessageComponentInterface
{
    /**
     * @var ConnectionInterface[]
     */
    private $clients;

    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(Registry $doctrine)
    {
        $this->clients = new ArrayCollection();
        $this->em = $doctrine->getManager();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->add($conn);

        echo 'Connection added';
    }

    public function onMessage(ConnectionInterface $from, $data)
    {
        $data = json_decode($data, true);

        $message = $this->createAndSaveMessage($data);

        foreach ($this->clients as $client) {
            $client->send($this->createResponseMessage($message));
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->removeElement($conn);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->close();
    }

    private function createResponseMessage(Message $message)
    {
        return json_encode(
            [
                'message_body' => $message->getBody(),
                'user_full_name' => $message->getUser()->getFullName(),
                'message_date' => $message->chatDate(),
                'channel_slug' => $message->getChannel()->getSlug(),
            ]
        );
    }

    private function createAndSaveMessage(array $data)
    {
        $em = $this->em;

        $user = $em->getRepository('Drivers\UserBundle\Entity\User')->find($data['user_id']);

        /**
         * @var Channel $channel
         */
        $channel = $em->getRepository('Drivers\ChatBundle\Entity\Channel')->findOneBy([
            'slug' => $data['channel_slug']
        ]);

        if (!($user instanceof User) || !($channel instanceof Channel)) {
            return false;
        }

        $message = new Message();
        $message
            ->setBody($data['message'])
            ->setUser($user)
            ->setDate(new \DateTime())
            ->setChannel($channel);

        $em->persist($message);
        $em->flush();

        return $message;
    }
} 