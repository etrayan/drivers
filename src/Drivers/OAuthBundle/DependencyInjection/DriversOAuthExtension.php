<?php

namespace Drivers\OAuthBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class DriversOAuthExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $vkontakteConfig = $config['vkontakte'];
        $container->setParameter('vkontakte', $vkontakteConfig);

        $facebookConfig = $config['facebook'];
        $container->setParameter('facebook', $facebookConfig);
//        $container->setParameter('vkontakte.redirect_uri', $vkontakteConfig['redirect_uri']);
//        $container->setParameter('vkontakte.app_id', $vkontakteConfig['client_id']);
//        $container->setParameter('vkontakte.redirect_uri', $vkontakteConfig['redirect_uri']);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
    }
}
