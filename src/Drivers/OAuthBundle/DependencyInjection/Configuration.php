<?php

namespace Drivers\OAuthBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('drivers_o_auth');

        $rootNode
            ->children()
                ->arrayNode('vkontakte')
                    ->children()
                        ->scalarNode('redirect_uri')->isRequired()->end()
                        ->scalarNode('app_id')->isRequired()->end()
                        ->scalarNode('api_secret')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('facebook')
                    ->children()
                        ->scalarNode('redirect_uri')->isRequired()->end()
                        ->scalarNode('app_id')->isRequired()->end()
                        ->scalarNode('api_secret')->isRequired()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
