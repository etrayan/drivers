<?php

namespace Drivers\OAuthBundle\DependencyInjection\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;

class VkontakteFactory extends AbstractFactory
{
    public function getPosition()
    {
        return 'form';
    }

    public function getKey()
    {
        return 'vkontakte';
    }

    protected function isRememberMeAware($config)
    {
        return false;
    }

    protected function getListenerId()
    {
        return 'vkontakte.security.authentication.listener';
    }

    protected function createAuthProvider(ContainerBuilder $container, $id, $config, $userProviderId)
    {
        $provider = 'security.authentication.provider.vkontakte.'.$id;
        $container
            ->setDefinition($provider, new DefinitionDecorator('social.security.authentication.provider'));
        ;

        return $provider;
    }
} 