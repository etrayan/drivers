<?php

namespace Drivers\OAuthBundle\Service\UserProvider;

use Drivers\UserBundle\Entity\User;
use Drivers\UserBundle\Entity\UserSource;

class FacebookUserProvider extends BaseUserProvider
{
    public function createFromApi()
    {
        $user = new User();
        $apiUser = $this->apiUser;

        $user
            ->setUid($apiUser['id'])
            ->setFirstName(isset($apiUser['first_name']) ? $apiUser['first_name'] : '')
            ->setLastName(isset($apiUser['last_name']) ? $apiUser['last_name'] : '')
            ->setSource(UserSource::FACEBOOK);

        return $user;
    }

    public function isValid()
    {
        $apiUser = $this->apiUser;

        return is_array($apiUser) && isset($apiUser['id']);
    }
} 