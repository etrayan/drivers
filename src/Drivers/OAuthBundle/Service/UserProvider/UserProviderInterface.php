<?php

namespace Drivers\OAuthBundle\Service\UserProvider;

use Drivers\UserBundle\Entity\User;

interface UserProviderInterface
{
    /**
     * @param array $apiUser
     * @return $this
     */
    public function setApiUser(array $apiUser);

    /**
     * @return User
     */
    public function createFromApi();

    /**
     * @return boolean
     */
    public function isValid();
} 