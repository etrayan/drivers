<?php

namespace Drivers\OAuthBundle\Service\UserProvider;


abstract class BaseUserProvider implements UserProviderInterface
{
    /**
     * @var array
     */
    protected $apiUser;

    public function setApiUser(array $apiUser)
    {
        $this->apiUser = $apiUser;

        return $this;
    }
} 