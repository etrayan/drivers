<?php

namespace Drivers\OAuthBundle\Security\Authentication\Provider;

use Drivers\OAuthBundle\Security\Authentication\Token\FacebookToken;
use Drivers\OAuthBundle\Security\Authentication\Token\SocialToken;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Drivers\OAuthBundle\Security\Authentication\Token\VkontakteToken;
use Drivers\UserBundle\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use \Doctrine\Common\Persistence\ObjectManager;
use \Doctrine\Common\Persistence\ObjectRepository;

class SocialAuthenticationProvider implements AuthenticationProviderInterface
{
    /**
     * @var ObjectRepository
     */
    private $repository;

    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(
        ManagerRegistry $registry
    ) {
        $this->em = $registry->getManager();
        $this->repository = $this->em->getRepository('Drivers\UserBundle\Entity\User');
    }

    public function authenticate(TokenInterface $token)
    {
        /**
         * @var User $tokenUser
         */
        $tokenUser = $token->getUser();

        $user = $this->repository->findOneBy([
            'uid' => $tokenUser->getUid(),
            'source' => $tokenUser->getSource()
        ]);

        if (!$user) {
            $user = $tokenUser;

            $em = $this->em;
            $em->persist($user);
            $em->flush();
        }

        $authenticatedToken = new SocialToken($user->getRoles());
        $authenticatedToken->setUser($user);

        return $authenticatedToken;
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof VkontakteToken || $token instanceof FacebookToken;
    }
} 