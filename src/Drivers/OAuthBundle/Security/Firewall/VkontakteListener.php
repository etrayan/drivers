<?php

namespace Drivers\OAuthBundle\Security\Firewall;

use Drivers\OAuthBundle\Service\UserProvider\UserProviderInterface;
use Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Drivers\OAuthBundle\Security\Authentication\Token\VkontakteToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Http\HttpUtils;
use VK\VK;
use Drivers\OAuthBundle\Helpers\VkontakteApiMethod;
use Symfony\Component\Routing\RouterInterface;

class VkontakteListener extends AbstractAuthenticationListener
{
    /**
     * @var VK
     */
    private $vkontakte;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var UserProviderInterface
     */
    private $vkontakteProvider;

    private $vkontakteRedirectUri;

    public function __construct(
        SecurityContextInterface $securityContext,
        AuthenticationManagerInterface $authenticationManager,
        SessionAuthenticationStrategyInterface $sessionStrategy,
        HttpUtils $httpUtils,
        $providerKey,
        AuthenticationSuccessHandlerInterface $successHandler,
        AuthenticationFailureHandlerInterface $failureHandler,
        array $options = array(),
        LoggerInterface $logger = null,
        EventDispatcherInterface $dispatcher = null,
        VK $vkontakte,
        RouterInterface $router,
        UserProviderInterface $vkontakteProvider,
        $vkontakteRedirectUri
    ) {
        parent::__construct(
            $securityContext,
            $authenticationManager,
            $sessionStrategy,
            $httpUtils,
            $providerKey,
            $successHandler,
            $failureHandler,
            $options,
            $logger,
            $dispatcher
        );

        $this->vkontakte = $vkontakte;
        $this->router = $router;
        $this->vkontakteProvider = $vkontakteProvider;
        $this->vkontakteRedirectUri = $vkontakteRedirectUri;
    }

    protected function attemptAuthentication(Request $request)
    {
        $code = $request->get('code', null);

        if (!$code) {
            throw new AuthenticationException();
        }

        $vkontakte = $this->vkontakte;

        $tokenInfo = $vkontakte->getAccessToken(
            $code,
            $this->router->generate($this->vkontakteRedirectUri, [], RouterInterface::ABSOLUTE_URL)
        );

        $content = $this->vkontakte->api(VkontakteApiMethod::GET_USERS, [
            'uids' => $tokenInfo['user_id']
        ]);

        if (isset($content['response']) && $user = array_shift($content['response'])) {
            $provider = $this->vkontakteProvider->setApiUser($user);

            if ($provider->isValid()) {
                $vkontakteToken = new VkontakteToken();
                $vkontakteToken->setUser($provider->createFromApi());

                return $this->authenticationManager->authenticate($vkontakteToken);
            }
        }

        throw new AuthenticationException();
    }
} 