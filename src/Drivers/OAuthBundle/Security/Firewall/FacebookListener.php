<?php

namespace Drivers\OAuthBundle\Security\Firewall;

use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Drivers\OAuthBundle\Security\Authentication\Token\FacebookToken;
use Symfony\Component\Security\Http\Firewall\AbstractAuthenticationListener;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Session\SessionAuthenticationStrategyInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Http\HttpUtils;
use Drivers\OAuthBundle\Service\UserProvider\UserProviderInterface;

class FacebookListener extends AbstractAuthenticationListener
{
    /**
     * @var \Facebook
     */
    private $facebook;

    /**
     * @var UserProviderInterface
     */
    private $facebookProvider;

    public function __construct(
        SecurityContextInterface $securityContext,
        AuthenticationManagerInterface $authenticationManager,
        SessionAuthenticationStrategyInterface $sessionStrategy,
        HttpUtils $httpUtils,
        $providerKey,
        AuthenticationSuccessHandlerInterface $successHandler,
        AuthenticationFailureHandlerInterface $failureHandler,
        array $options = array(),
        LoggerInterface $logger = null,
        EventDispatcherInterface $dispatcher = null,
        \Facebook $facebook,
        UserProviderInterface $facebookProvider
    ) {
        parent::__construct(
            $securityContext,
            $authenticationManager,
            $sessionStrategy,
            $httpUtils,
            $providerKey,
            $successHandler,
            $failureHandler,
            $options,
            $logger,
            $dispatcher
        );

        $this->facebook = $facebook;
        $this->facebookProvider = $facebookProvider;

    }

    protected function attemptAuthentication(Request $request)
    {
        $user = $this->facebook->api('/me');

        $provider = $this->facebookProvider->setApiUser($user);

        if ($provider->isValid()) {
            $token = new FacebookToken();
            $token->setUser($provider->createFromApi());

            return $this->authenticationManager->authenticate($token);
        }

        throw new AuthenticationException();
    }
} 