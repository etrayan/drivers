<?php

namespace Drivers\OAuthBundle\Twig;

use Symfony\Component\Routing\RouterInterface;
use VK\VK;

class SocialExtension extends \Twig_Extension
{
    /**
     * @var \Facebook
     */
    private $facebook;

    /**
     * @var VK
     */
    private $vkontakte;

    /**
     * @var RouterInterface
     */
    private $router;

    private $facebookRedirectUri;

    private $vkontakteRedirectUri;

    /**
     * @param \Facebook $facebook
     * @param VK $vkontakte
     * @param RouterInterface $router
     * @param string $vkontakteRedirectUri
     * @param string $facebookRedirectUri
     */
    public function __construct(
        \Facebook $facebook,
        VK $vkontakte,
        RouterInterface $router,
        $vkontakteRedirectUri,
        $facebookRedirectUri
    ) {
        $this->facebook = $facebook;
        $this->vkontakte = $vkontakte;
        $this->router = $router;
        $this->vkontakteRedirectUri = $vkontakteRedirectUri;
        $this->facebookRedirectUri = $facebookRedirectUri;
    }

    public function getName()
    {
        return 'social';
    }

    public function getFunctions()
    {
        return [
            'vkontakte_link' => new \Twig_Function_Method($this, 'getVkontakteLink'),
            'facebook_link' => new \Twig_Function_Method($this, 'getFacebookLink'),
        ];
    }

    public function getVkontakteLink()
    {
        return $this->vkontakte->getAuthorizeURL(
            '',
            $this->router->generate($this->vkontakteRedirectUri, [], RouterInterface::ABSOLUTE_URL)
        );
    }

    public function getFacebookLink()
    {
        return $this->facebook->getLoginUrl([
            'redirect_uri' => $this->router->generate($this->facebookRedirectUri, [], RouterInterface::ABSOLUTE_URL)
        ]);
    }
} 