<?php

namespace Drivers\OAuthBundle;

use Drivers\OAuthBundle\DependencyInjection\Security\Factory\FacebookFactory;
use Drivers\OAuthBundle\DependencyInjection\Security\Factory\VkontakteFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DriversOAuthBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new VkontakteFactory());
        $extension->addSecurityListenerFactory(new FacebookFactory());
    }
}
