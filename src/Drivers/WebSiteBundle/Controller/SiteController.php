<?php

namespace Drivers\WebSiteBundle\Controller;

use Drivers\UserBundle\Entity\User;
use Drivers\WebSiteBundle\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class SiteController extends Controller
{
    /**
     * @Route("/registration", name="registration")
     * @Template()
     */
    public function registrationAction(Request $request)
    {
        $user = new User();
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);

        $form = $this->createForm(new RegistrationFormType(), $user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('login'));
        }

        return [
            'form'  =>  $form->createView(),
            'user'  =>  $user
        ];
    }

    /**
     * @Route("/", name="home")
     * @Template()
     */
    public function homeAction()
    {
        return [];
    }
}
