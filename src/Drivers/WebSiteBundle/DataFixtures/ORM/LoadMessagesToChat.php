<?php

namespace Drivers\WebSiteBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Drivers\ChatBundle\Entity\Channel;
use Drivers\ChatBundle\Entity\Message;
use Drivers\UserBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadMessagesToChat implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $channel = (new Channel())
            ->setName('Фикстурный чат')
            ->setSlug('fixture-channel');

        $user = (new User())
            ->setEmail('eduardtrayan@gmail.com')
            ->setFirstName('Эдуард')
            ->setLastName('Троян')
            ->setPassword(111111);

        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());

        $user->setPassword($password);

        $manager->persist($channel);
        $manager->persist($user);

        for($i=0;$i<100;$i++) {
            $message = (new Message())
                ->setBody(
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                     Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                      It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
                )
                ->setChannel($channel)
                ->setDate(new \DateTime())
                ->setUser($user);

            $manager->persist($message);
        }

        $manager->flush();
    }
} 