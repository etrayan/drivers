<?php

namespace Drivers\WebSiteBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'email',
                null,
                [
                    'constraints' =>
                    [
                        new NotBlank(),
                        new Email()
                    ],
                    'required' => true,
                    'attr' =>
                    [
                        'class' => 'input-xlarge',
                        'placeholder' => 'Email',
                    ]
                ]
            )
            ->add(
                'password',
                null,
                [
                    'label' =>  'Пароль',
                    'constraints' =>
                    [
                        new NotBlank(),
                    ],
                    'required' => true,
                    'attr' =>
                    [
                        'class' => 'input-xlarge',
                        'placeholder' => 'Пароль',
                    ]
                ]
            )
            ->add(
                'save',
                'submit',
                [
                    'label' =>  'Зарегистрироваться',
                    'attr' =>
                    [
                        'class' => 'btn btn-lg btn-success btn-block'
                    ]
                ]
            );
    }

    public function getName()
    {
        return 'registration';
    }
} 