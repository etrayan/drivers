<?php

namespace Drivers\AdminBundle\Entity;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class Channel extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'Имя чата'))
            ->add('slug', 'text', array('label' => 'Уникальный идентификатор чата'))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('slug')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array('name' => 'Просмотреть'),
                    'edit' => array(),
                    'delete' => array('title' => 'Удалить', 'name' => 'Удалить', 'label' => 'Удалить', 'content' => 'Удалить', 'text' => 'Удалить'),
                )
            ))
        ;
    }
} 